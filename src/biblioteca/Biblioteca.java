package biblioteca;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

	private final String INSERT = "INSERT INTO `biblioteca`.`biblioteca` (`nombre`) VALUES ('%s');";

	private String nombre;

	private int idBiblioteca;

	private List<Usuario> listaUsuario;
	private List<Libro> listaLibro;
	private List<Prestamo> listaPrestamo;

	public Biblioteca(String nombre, int idBiblioteca) {
		super();
		this.nombre = nombre;
		this.idBiblioteca = idBiblioteca;
		listaUsuario = new ArrayList<Usuario>();
		listaLibro = new ArrayList<Libro>();
		listaPrestamo = new ArrayList<Prestamo>();
	}

	protected boolean addLibro(Libro libro) {
		return listaLibro.add(libro);
	}

	protected boolean addUsuario(Usuario usuario) {
		return listaUsuario.add(usuario);
	}

	protected boolean addPrestamo(Prestamo prestamo) {
		return listaPrestamo.add(prestamo);
	}

	public String getInsert() {
		return String.format(INSERT,  nombre);
	}

	public int getId() {
		return this.idBiblioteca;
	}

	public String getnombre() {
		return this.nombre;
	}

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public List<Libro> getListaLibro() {
		return listaLibro;
	}

	public List<Prestamo> getListaPrestamo() {
		return listaPrestamo;
	}

}

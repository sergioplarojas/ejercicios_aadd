package biblioteca;

public class Libro {

	private final String INSERT = "INSERT INTO `biblioteca`.`libro` "
			+ "(`idlibro`,`titulo`,`autor`,`genero`,`id_biblioteca`) VALUES (%d,'%s','%s','%s',%d);";

	private int idLibro;

	private String titulo, autor, genero;

	public Libro(int idLibro, String titulo, String autor, String genero) {
		super();
		this.idLibro = idLibro;
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
	}

	public String getInsert(int idBiblioteba) {
		return String.format(INSERT, idLibro, titulo, autor, genero, idBiblioteba);
	}

	public int getIdLibro() {
		return idLibro;
	}

}

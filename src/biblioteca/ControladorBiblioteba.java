package biblioteca;

import java.sql.ResultSet;

import mSQLUtils.ConectionJDBC;

public class ControladorBiblioteba {
	private final String USER = "java";
	private final String PASS = "Java1";

	private ConectionJDBC conectionJDBC;
	private Biblioteca biblioteca;

	public ControladorBiblioteba(Biblioteca biblioteca) {
		super();
		this.conectionJDBC = new ConectionJDBC("jdbc:mysql://localhost:3306/biblioteca", USER, PASS);
		conectionJDBC.connect();
		this.biblioteca = biblioteca;
	}

	public ResultSet insertarBiblioteca() {
		System.out.println("Consulta: " + biblioteca.getInsert());
		return conectionJDBC.execSQL(biblioteca.getInsert());
	}

	public ResultSet insertarLibro(Libro libro) {
		if (biblioteca.addLibro(libro)) {
			return conectionJDBC.execSQL(libro.getInsert(biblioteca.getId()));
		}

		return null;
	}

	public ResultSet insertarPrestamo(Prestamo prestamo) {
		if (biblioteca.addPrestamo(prestamo)) {
			return conectionJDBC.execSQL(prestamo.getInsert());
		}
		return null;
	}

	public ResultSet insertarUsuario(Usuario usuario) {
		if (biblioteca.addUsuario(usuario)) {
			return conectionJDBC.execSQL(usuario.getInsert());
		}
		return null;
	}
}

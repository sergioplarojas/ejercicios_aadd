package biblioteca;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Prestamo {

	private final String INSERT = "INSERT INTO `biblioteca`.`prestamo_libro_usuario`"
			+ "(`id_libro`,`id_usuario`,`fecha_inicio`,`fecha_fin`)" + "VALUES(%d,%d,'%s','%s');";

	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private Usuario usuario;
	private Libro libro;
	private Date fecha_inicio, fecha_fin;

	
	public Prestamo(Usuario usuario, Libro libro, Date fecha_inicio, Date fecha_fin) {
		super();
		this.usuario = usuario;
		this.libro = libro;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
	}
	
	public String getInsert() {
		String stringFechaInicio = DATE_FORMAT.format(fecha_inicio);
		String stringFechaFin = DATE_FORMAT.format(fecha_fin);

		return String.format(INSERT, libro.getIdLibro(), usuario.getIdUsuario(), stringFechaInicio, stringFechaFin);
	}



}

package biblioteca;

public class Usuario {
	private final String INSERT = "INSERT INTO `biblioteca`.`usuario` (`idusuario`,`nombre`) VALUES (%d,'%s');";

	private int idUsuario;

	private String nombre;

	public Usuario(int idUsuario, String nombre) {
		super();
		this.idUsuario = idUsuario;
		this.nombre = nombre;
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}

	public String getInsert() {
		return String.format(INSERT, idUsuario, nombre);
	}

}

package biblioteca;

import java.util.Calendar;
import java.util.Date;

import mSQLUtils.RandomDate;
import mSQLUtils.RandomInt;
import mSQLUtils.RandomString;

public class BibliotecaAleatorio {
	private int numeroFilas;

	private RandomString randomString;
	private RandomInt randomInt;
	private RandomDate randomDate;

	private Date fechaMinima;
	private Date fechaMaxima;

	private Biblioteca biblioteca;

	private ControladorBiblioteba controladorBiblioteba;

	public BibliotecaAleatorio(int numeroFilas) {
		this.numeroFilas = numeroFilas;
		this.fechaMinima = getFechaMinima();
		this.fechaMaxima = new Date();
		this.randomString = new RandomString(8);
		this.randomInt = new RandomInt(100, 1000);
		this.randomDate = new RandomDate(fechaMinima, fechaMaxima);

		this.biblioteca = new Biblioteca(randomString.nextString(), 1);
		controladorBiblioteba = new ControladorBiblioteba(biblioteca);
	}

	private Date getFechaMinima() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1988);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public void insertarDatos() {
		insertarBiblioteba();
		insertarUsuarios();
		insertarLibros();
		insertarPrestamos();
	}

	private void insertarBiblioteba() {
		controladorBiblioteba.insertarBiblioteca();
	}

	private void insertarUsuarios() {
		for (int i = 0; i < numeroFilas; i++) {
			int idUsuario = randomInt.nextInt();
			String nombre = randomString.nextString();

			Usuario usuario = new Usuario(idUsuario, nombre);

			controladorBiblioteba.insertarUsuario(usuario);
		}
	}

	private void insertarLibros() {
		for (int i = 0; i < numeroFilas; i++) {
			int idLibro = randomInt.nextInt();
			String titulo = randomString.nextString();
			String autor = randomString.nextString();
			String genero = randomString.nextString();

			Libro libro = new Libro(idLibro, titulo, autor, genero);

			controladorBiblioteba.insertarLibro(libro);
		}
	}

	private void insertarPrestamos() {
		for (int i = 0; i < numeroFilas; i++) {
			Usuario usuario = biblioteca.getListaUsuario().get(i);
			Libro libro = biblioteca.getListaLibro().get(i);
			Date fecha_inicio = randomDate.nextDate();
			Date fecha_fin = fechaMaxima;

			Prestamo prestamo = new Prestamo(usuario, libro, fecha_inicio, fecha_fin);

			controladorBiblioteba.insertarPrestamo(prestamo);
		}
	}
}

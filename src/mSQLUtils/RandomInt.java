package mSQLUtils;

import java.util.concurrent.ThreadLocalRandom;

public class RandomInt {
	private int max, min;

	public RandomInt(int min, int max) {
		super();
		this.max = max;
		this.min = min;
	}

	public int nextInt() {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
}

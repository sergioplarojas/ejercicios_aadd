package mSQLUtils;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class RandomDate {
	Date dateMin, dateMax;

	public RandomDate(Date dateMin, Date dateMax) {
		super();
		this.dateMin = dateMin;
		this.dateMax = dateMax;
	}

	public Date nextDate() {
		return new Date(ThreadLocalRandom.current().nextLong(dateMin.getTime(), dateMax.getTime()));
	}
}

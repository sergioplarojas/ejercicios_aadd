package mSQLUtils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ConectionJDBC implements Serializable {

	// init database constants
	private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
	// private static final String DATABASE_URL =
	// "jdbc:mysql://localhost:3306/library";
	// private static final String USERNAME = "root";
	// private static final String PASSWORD = "1234";
	private static final String MAX_POOL = "250"; // set your own limit

	private static final long serialVersionUID = 4971075361003058584L;

	// init connection object
	private Connection connection;
	// init properties object
	private Properties properties;

	private String databaseURL, username, password;

	public ConectionJDBC(String databaseUrl, String username, String password) {
		this.databaseURL = databaseUrl;
		this.username = username;
		this.password = password;

	}

	// create properties
	private Properties getProperties() {
		if (properties == null) {
			properties = new Properties();
			properties.setProperty("user", username);
			properties.setProperty("password", password);
			properties.setProperty("MaxPooledStatements", MAX_POOL);
			properties.setProperty("database_name", "library");
		}
		return properties;
	}

	// Conecta a la base de datos
	public Connection connect() {
		if (connection == null) {
			try {
				Class.forName(DATABASE_DRIVER);
				connection = DriverManager.getConnection(databaseURL, getProperties());
			} catch (ClassNotFoundException | SQLException e) {
				// Java 7+
				e.printStackTrace();
			}
		}
		return connection;
	}

	// "Desconecta" de la bd
	public void disconnect() {
		if (connection != null) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ResultSet execSQL(String sql) {

		connect();
		ResultSet rs = null;

		try {

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.execute();

			rs = statement.getResultSet();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// disconnect();
		}

		return rs;

	}

	// public static void main(String[] args) {
	// // TODO Auto-generated method stub
	// // it will not create a connection
	//
	// ConectionJDBC test = new ConectionJDBC();
	//
	// try {
	// test.connect();
	// ResultSet rs = test.execSQL("INSERT INTO ejercicio1.person\r\n" +
	// "VALUES\r\n" + "(4,\r\n" + "\"Pepe\",\r\n"
	// + "\"Carlos Haya\");");
	// } finally {
	// test.disconnect();
	// }
	//
	// }
}
